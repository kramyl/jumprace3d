using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ContestantMotor))]
public class AIBehavior : MonoBehaviour
{
    [SerializeField, Header("Settings")]
    private float minRemainingDistToStop = 0.2f;
    [SerializeField]
    private float maxRemainingDistToStop = 2f;
    [SerializeField]
    private float chanceToStop = 0.5f;

    ContestantMotor contestantMotor;
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Transform lastTarget;
    [SerializeField]
    private int currentPathIndex = 0;
    private float remainingDistToStop;
    private bool isDone = false;
    private bool isStarted = false;

    private void Start()
    {
        contestantMotor = GetComponent<ContestantMotor>();
        contestantMotor.onChangedLastLandedTrampoline += OnChangedLastLandedTrampoline;
        StartCoroutine(MoveToTarget());
    }

    private void OnChangedLastLandedTrampoline(int currentTrampoline)
    {
        lastTarget = target;

        currentPathIndex = currentTrampoline - 1;

        if (Random.Range(0f,1f) <= chanceToStop && isStarted)
        {
            currentPathIndex = currentTrampoline;
        }
       
        if (currentPathIndex > 0)
        {
            target = TrampolinesManager.Instance.FindTrampolineTransform(currentPathIndex);
        }
        else
        {
            var newTarget = TrampolinesManager.Instance.GetFinishLine();
            if (target != newTarget)
            {
                target = newTarget;
            }
            else
            {
                isDone = true;
            }
        }
        remainingDistToStop = Random.Range(minRemainingDistToStop, maxRemainingDistToStop);
        if (!isStarted)
            isStarted = true;
    }

    private void GetNewTarget()
    {
        
    }

    IEnumerator MoveToTarget()
    {
        yield return StartCoroutine(CheckIfRaceStarted());
        contestantMotor.EnablePhysics(true);
        while (!isDone)
        {
            while (target != null)
            {
                var newAiPos = transform.position;
                newAiPos.y = 0;
                var newTargetPos = target.position;
                newTargetPos.y = 0;

                var distToTarget = Vector3.Distance(newAiPos, newTargetPos);
                var canMove = distToTarget > remainingDistToStop;
                if (lastTarget != target && canMove)
                {
                    contestantMotor.SetIsMoving(true);
                    var relativeDir = transform.InverseTransformPoint(newTargetPos).normalized;
                    var turnDir = 0f;
                    if (relativeDir.x >= 0.01f)
                        turnDir = 1f;
                    else if(relativeDir.x <= -0.01f)
                        turnDir = -1f;

                    contestantMotor.SetTurnDir(turnDir);
                }
                else
                {
                    contestantMotor.SetIsMoving(false);
                    target = null;
                }
                yield return null;
            }
            yield return null;
        }
        
    }

    IEnumerator CheckIfRaceStarted()
    {
        while (!GameHandler.Instance.IsRaceStarted)
        {
            yield return null;
        }
    }


}

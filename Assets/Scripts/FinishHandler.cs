using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishHandler : MonoBehaviour
{
    #region Singleton
    public static FinishHandler Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

    }

    private void OnDestroy()
    {
        Instance = null;
    }
    #endregion

    [SerializeField]
    private GameObject vfx;

    public void ShowVfx() => vfx.SetActive(true);
}

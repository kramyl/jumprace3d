﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public static CameraFollow instance;

    [SerializeField, Header("Settings")]
    private Transform target;
    [SerializeField]
    private float smoothSpeed = 0.125f;
    [SerializeField]
    private Vector3 offset = new Vector3(0, 12, -10);
    [SerializeField, Tooltip("To disable clamp in specific axis input negative numbers")]
    private Vector3 clamp = Vector3.one * -1f;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void FixedUpdate()
    {
        if (target == null) return;
        ApplyOffset();
    }

    private void ApplyOffset()
    {
        Vector3 newPosition = GetNewCamPos();
        transform.position = newPosition = Vector3.Lerp(transform.position, newPosition, smoothSpeed * Time.deltaTime);
    }

    private Vector3 GetNewCamPos()
    {
        var newPosition = target.position + target.TransformVector(offset);
        if (clamp.x > 0)
            newPosition.x = Mathf.Clamp(newPosition.x, -clamp.x, clamp.x);
        if (clamp.y > 0)
            newPosition.y = Mathf.Clamp(newPosition.y, -clamp.y, clamp.y);
        if (clamp.z > 0)
            newPosition.z = Mathf.Clamp(newPosition.z, -clamp.z, clamp.z);
        transform.LookAt(target.position);
        return newPosition;
    }

    public void SetTarget(Transform value)
    {
        target = value;
        if (value != null)
        {
            transform.position = GetNewCamPos();
        }
    }

    public void SetOffset(Vector3 value) => offset = value;

    public Vector3 GetOffset() => offset;

    public void SetClamp(Vector3 value) => clamp = value;

    public void SetSmoothSpeed(float value) => smoothSpeed = value;

    public Vector3 GetClamp() => clamp;
}

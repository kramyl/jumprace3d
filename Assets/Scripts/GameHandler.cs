using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{
    #region Singleton
    public static GameHandler Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        Instance = null;
    }
    #endregion

    [SerializeField, Header("Settings")]
    private int numberOfAiContestant = 5;
    [SerializeField]
    private List<string> aiNames = new List<string>();
    [SerializeField]
    private List<Color> aiColors = new List<Color>();

    [SerializeField, Header("Prefabs References")]
    private GameObject prefabPlayer;
    [SerializeField]
    private GameObject prefabAI;
    [SerializeField, Header("Other References")]
    private TrampolineFeedbackSettings trampolineFeedbacksSettings;

    private ContestantMotor playerMotor;
    [SerializeField]
    private List<ContestantMotor> contestants = new List<ContestantMotor>();

    private const string playerName = "You";

    private bool isDone;

    private void Start()
    {
        SpawnContestants();

        playerMotor.onOutOfBounce += OnPlayerOutOfBounce;
        playerMotor.onChangedLastLandedTrampoline += OnChangedPlatform;
        playerMotor.onFeedbackTrampoline += OnFeedbackTrampoline;
    }

    private void Update()
    {
        if(!isDone)
            CheckPosition();
    }

    private void CheckPosition()
    {
        contestants = contestants.OrderBy(x => x.GetCurrentPlatform()).ToList();
        int playerPos = 0;
        for (int i = 0; i < contestants.Count; i++)
        {
            var isPlayer = contestants[i].ContestantName == playerName;
            if (i < 3)
            {
                GameplayUiHandler.Instance.SetContestantPositions(contestants[i].ContestantName, i, isPlayer);
            }
         
            if (isPlayer && i > 2)
            {
                playerPos = i;
                break;
            }
        }
        if(playerPos + 1 > 3)
            GameplayUiHandler.Instance.SetContestantPositions(playerMotor.ContestantName, playerPos, true);
    }

    private void SpawnContestants()
    {
        var pathLength = TrampolinesManager.Instance.GetTrampolineLength();
        playerMotor = InstantiateContestant(prefabPlayer, TrampolinesManager.Instance.FindTrampolineTransform(pathLength));
        playerMotor.ContestantName = playerName;
        contestants.Add(playerMotor);

        CameraFollow.instance.SetTarget(playerMotor.transform);

        var tempAiNames = new List<string>(aiNames);
        var tempAiColors = new List<Color>(aiColors);
        for (int i = 1; i <= numberOfAiContestant; i++)
        {
            var resultAiName = tempAiNames[Random.Range(0, tempAiNames.Count)];
            tempAiNames.Remove(resultAiName);
            var resultAiColor = tempAiColors[Random.Range(0, tempAiColors.Count)];
            tempAiColors.Remove(resultAiColor);
            var newAiContestant = InstantiateContestant(prefabAI, TrampolinesManager.Instance.FindTrampolineTransform(pathLength - i));
            newAiContestant.ContestantName = resultAiName;
            newAiContestant.GetComponentInChildren<SkinnedMeshRenderer>().material.color = resultAiColor;
            contestants.Add(newAiContestant);
        }
    }

    private string[] GetContestantsResults()
    {
        var names = new string[5];
        CheckPosition();
        for (int i = 0; i < names.Length; i++)
        {
            names[i] = contestants[i].ContestantName;
        }
        return names;
    }

    private ContestantMotor InstantiateContestant(GameObject prefab, Transform spawnTransform)
    {
        var offsetY = 1.4f;
        var spawnPoint = spawnTransform.position;
        spawnPoint.y += offsetY;
        return Instantiate(prefab, spawnPoint, spawnTransform.rotation).GetComponent<ContestantMotor>();
    }

    private void OnPlayerOutOfBounce()
    {
        CameraFollow.instance.SetTarget(null);
        isDone = true;
        GameplayUiHandler.Instance.ShowRecap(false, GetContestantsResults());
        GameplayUiHandler.Instance.onClickedTapToContinue += delegate { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); };
    }

    private void OnChangedPlatform(int currentPlatform)
    {
        GameplayUiHandler.Instance.SetProgressbarLevel((float)(TrampolinesManager.Instance.GetTrampolineLength() - currentPlatform) / TrampolinesManager.Instance.GetTrampolineLength());
        if(SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCount)
            GameplayUiHandler.Instance.onClickedTapToContinue += delegate { SceneManager.LoadScene(1); };
        else
            GameplayUiHandler.Instance.onClickedTapToContinue += delegate { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); };

        if (currentPlatform == 0)
        {
            isDone = true;
            StartCoroutine(ShowLevelCompleted());
        }
    }

    IEnumerator ShowLevelCompleted()
    {
        CheckPosition();
        FinishHandler.Instance.ShowVfx();
        yield return new WaitForSeconds(1.5f);
        GameplayUiHandler.Instance.ShowRecap(true, GetContestantsResults());
    }

    private void OnFeedbackTrampoline(string feedback, Color color)
    {
        GameplayUiHandler.Instance.SetTextFeedback(feedback, color);
    }

    public TrampolineFeedbackSettings GetTrampolineFeedbacksSettings() => trampolineFeedbacksSettings;

    public bool IsRaceStarted { get; set; }
    public ContestantMotor GetPlayerMotor() => playerMotor;

}

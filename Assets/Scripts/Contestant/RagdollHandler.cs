using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollHandler : MonoBehaviour
{
    private List<Collider> colliders = new List<Collider>();
    private List<Rigidbody> rigidbodies = new List<Rigidbody>();
    private Rigidbody rigid;
    private Collider col;
    private FixedJoint fixedJoint;

    public bool isRagdollEnabled = false;
    public float totalMass;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        fixedJoint = GetComponentInChildren<FixedJoint>();
        GetAllColliders();
        EnableRagdoll(isRagdollEnabled);
    }

    private void GetAllColliders()
    {
        var newColliders = GetComponentsInChildren<Collider>();
        foreach (var newCollider in newColliders)
        {
            if (newCollider.gameObject != gameObject)
            {
                colliders.Add(newCollider);
                rigidbodies.Add(newCollider.attachedRigidbody);
                totalMass += newCollider.attachedRigidbody.mass;
            }
        }
    }

    public bool IsRagdollAsleep()
    {
        return colliders[0].attachedRigidbody.velocity.magnitude <= 0.5f;
    }

    public void EnableRagdoll(bool isRagdollEnabled = true)
    {
        for (int i = 0; i < colliders.Count; i++)
        {
            colliders[i].enabled = isRagdollEnabled;
            rigidbodies[i].isKinematic = !isRagdollEnabled;
        }
        col.enabled = !isRagdollEnabled;
        fixedJoint.connectedBody = isRagdollEnabled ? rigid : null;
        GetComponent<Animator>().enabled = !isRagdollEnabled;
        this.isRagdollEnabled = isRagdollEnabled;
    }

    public void AddForce(Vector3 force)
    {
        colliders[0].attachedRigidbody.AddForce(force * totalMass, ForceMode.Impulse);
    }

    public void ForceStop()
    {
        rigid.velocity = Vector3.zero;
        colliders[0].attachedRigidbody.velocity = Vector3.zero;
    }

    public Transform GetRoot() => colliders[0].transform;
}
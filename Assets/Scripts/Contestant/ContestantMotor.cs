using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(RagdollHandler))]
[RequireComponent(typeof(Animator))]
public class ContestantMotor : MonoBehaviour
{
    public delegate void OnOutOfBounce();
    public event OnOutOfBounce onOutOfBounce;

    public delegate void OnChangedLastLandedTrampoline(int currentTrampoline);
    public event OnChangedLastLandedTrampoline onChangedLastLandedTrampoline;

    public delegate void OnFeedbackTrampoline(string feedback, Color colorFeedback);
    public event OnFeedbackTrampoline onFeedbackTrampoline;

    [SerializeField]
    private string contestantName;
    [SerializeField, Header("Settings")]
    private float speed = 5f;
    [SerializeField]
    private float turnSpeed = 10f;
    [SerializeField]
    private LayerMask layerGrounds;

    private Rigidbody rigid;
    private Animator anim;
    private RagdollHandler ragdollHandler;

    private int currentPlatform = 0;

    private float turnDirection;
    private float highestPoint;

    private Vector3 lastJumpForce;

    private bool isGrounded;
    private bool isMoving;

    private const string TAG_OBSTACLE = "Obstacle";
    private const string TAG_FINISH = "Finish";
    private const string TAG_TRAMPOLINE = "Trampoline";

    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        ragdollHandler = GetComponent<RagdollHandler>();
        EnablePhysics(false);
    }

    private void FixedUpdate()
    {
        CheckIsGrounded();
        Move();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag(TAG_TRAMPOLINE))
        {
            var platform = collision.collider.gameObject.GetComponentInParent<Trampoline>();
            if (platform != null)
            {

                var getResult = platform.GetForce(collision.contacts[0].point, highestPoint - transform.position.y);
                Jump(getResult.Item1);
                onFeedbackTrampoline?.Invoke(getResult.Item2, getResult.Item3);
          
                if (platform.IsPath()) 
                { 
                    currentPlatform = platform.GetNumber();
                    onChangedLastLandedTrampoline?.Invoke(currentPlatform);
                }
               
            }
        }
        else if (collision.collider.CompareTag(TAG_OBSTACLE))
        {
            ragdollHandler.EnableRagdoll();
            onOutOfBounce?.Invoke();
        }
        else if (collision.collider.CompareTag(TAG_FINISH))
        {
            currentPlatform = 0;
            onChangedLastLandedTrampoline?.Invoke(currentPlatform);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(TAG_OBSTACLE))
        {
            onOutOfBounce?.Invoke();
        }
    }

    private void CheckIsGrounded()
    {
        var origin = transform.position;
        origin.y += 0.1f;
        isGrounded = Physics.Raycast(origin, Vector3.down, 0.2f, layerGrounds);
        anim.SetBool("IsGrounded", isGrounded);
        if (transform.position.y > highestPoint)
        {
            highestPoint = transform.position.y;
        }
    }

    private void Jump(float force, int animIndexResult = - 1)
    {
        highestPoint = transform.position.y;
        var velocity = rigid.velocity;
        velocity.y = 0;
        rigid.velocity = velocity;

        lastJumpForce = Vector3.up * force;
        Vector3 directionForce = lastJumpForce;
        rigid.AddForce(directionForce, ForceMode.Impulse);
       
        if(animIndexResult == -1)
            animIndexResult = Random.Range(1, 3);
        switch (animIndexResult)
        {
            case 0:
                anim.SetTrigger("Jump");
                break;
            case 1:
                anim.SetTrigger("ForwardFlip");
                break;
            case 2:
                anim.SetTrigger("BackFlip");
                break;
        }
    }

    private void Move()
    {
        if (isMoving && !isGrounded)
        {
            var velocity = transform.forward * speed;
            velocity.y = rigid.velocity.y;
            rigid.velocity = velocity;
            rigid.angularVelocity = Vector3.up * turnDirection * turnSpeed;
        }
        else
        {
            rigid.velocity = Vector3.Lerp(rigid.velocity, new Vector3(0, rigid.velocity.y, 0), 5 * Time.deltaTime);
            rigid.angularVelocity = Vector3.zero;
        }
    }

    public void SetTurnDir(float turnDirection) => this.turnDirection = turnDirection;
    public void SetIsMoving(bool value) => isMoving = value;
    public Vector3 GetLastJumpForce() => lastJumpForce;
    public bool IsGrounded() => isGrounded;

    public void EnablePhysics(bool isEnabled) => rigid.isKinematic = !isEnabled;

    public string ContestantName { get => contestantName; set => contestantName = value; }
    public int GetCurrentPlatform() => currentPlatform;
}

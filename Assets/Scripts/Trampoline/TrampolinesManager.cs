using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolinesManager : MonoBehaviour
{
    #region Singleton
    public static TrampolinesManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

    }

    private void OnDestroy()
    {
        Instance = null;
    }
    #endregion


    [SerializeField]
    private Trampoline[] trampolines;
    [SerializeField]
    private Transform finishLine;
    private LineRenderer lineRenderer;
    private void Start()
    {
        lineRenderer = GetComponentInChildren<LineRenderer>();

        lineRenderer.positionCount = trampolines.Length;
        for (int i = 0; i < trampolines.Length; i++)
        {
            trampolines[i].SetNumber(trampolines.Length - i);
            lineRenderer.SetPosition(i, trampolines[i].transform.position);
        }
    }

    public int GetTrampolineLength() => trampolines.Length;

    public Transform FindTrampolineTransform(int number)
    {
        Transform result = null;
        foreach (var trampoline in trampolines)
        {
            if(trampoline.GetNumber() == number)
                result = trampoline.transform;
        }
        return result;
    }

    public Transform GetFinishLine() => finishLine;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Trampoline : MonoBehaviour
{
    [SerializeField, Header("Force Settings")]
    private float forcePerfect = 12f;
    [SerializeField]
    private float forceGreat = 10f;
    [SerializeField]
    private float forceGood = 8f;

    [SerializeField, Header("Accuracy Settings")]
    private float rangePerfect = 0.2f;
    [SerializeField]
    private float rangeGreat = 0.5f;
    [SerializeField]
    private float rangeGood = 1f;

    [SerializeField, Header("Long Jump Settings")]
    private float jumpDistance = 50f;
    [SerializeField]
    private float maxLongJumpForce = 60f;
    [SerializeField, Range(0, 1)]
    private float forceToApply = 0.5f;

    [SerializeField, Header("Other Settings")]
    private bool isBreakable = false;
    [SerializeField]
    private bool isPath = false;

    [SerializeField, Header("References")]
    private Animator animTrampoline;
    [SerializeField]
    private Animator animWings;
    [SerializeField]
    private Rigidbody[] trampolinePieces;
    [SerializeField]
    private Collider mainCollider;
    [SerializeField]
    private TrampolineUiHandler trampolineUiHandler;

    private TrampolineFeedbackSettings feedbackSettings;
    private int number;

    private void Start()
    {
        feedbackSettings = GameHandler.Instance.GetTrampolineFeedbacksSettings();
    }

    public (float, string, Color) GetForce(Vector3 contactPoint, float jumpDistance)
    {
        var origin = new Vector3(transform.position.x, 0f, transform.position.z);
        contactPoint = new Vector3(contactPoint.x, 0f, contactPoint.z);

        var distanceFromCenter = Vector3.Distance(origin, contactPoint);

        if(!isBreakable)
            animTrampoline.SetTrigger("Bounce");
        else
        {
            if (trampolinePieces.Length > 0)
            {
                animWings.enabled = false;
                mainCollider.enabled = false;
                foreach (var rigid in trampolinePieces)
                {
                    rigid.transform.parent = null;
                    rigid.isKinematic = false;

                    var force = Vector3.up;
                    var rndNumber = Random.Range(0, 3);
                    if (rndNumber == 0)
                    {
                        force = Vector3.forward;
                    }
                    else if (rndNumber == 1)
                    {
                        force = Vector3.right;
                    }

                    rigid.AddForce(force * Random.Range(-1f, 1f), ForceMode.Impulse);
                    Destroy(rigid.gameObject, 3f);
                }
            }
        }

        if (jumpDistance >= this.jumpDistance)
        {
            if (jumpDistance > maxLongJumpForce)
                jumpDistance = maxLongJumpForce;
            return (jumpDistance * forceToApply, feedbackSettings.feedbackLongJump, feedbackSettings.colorLongJump);
        }
        else
        {
            if (distanceFromCenter < rangePerfect)
                return (forcePerfect, feedbackSettings.feedbackPerfect, feedbackSettings.colorPerfect);
            else if (distanceFromCenter < rangeGreat)
                return (forceGreat, feedbackSettings.feedbackGreat, feedbackSettings.colorGreat);
            else
                return (forceGood, feedbackSettings.feedbackGood, feedbackSettings.colorGood);
        }
    }

    private void OnDrawGizmosSelected()
    {
        var origin = new Vector3(transform.position.x, transform.position.y + 1.375f, transform.position.z);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(origin, rangePerfect);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(origin, rangeGreat);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(origin, rangeGood);
    }

    public void SetNumber(int number)
    {
        this.number = number;
        trampolineUiHandler.SetTextNumber(number);
    }

    public int GetNumber() => number;

    public bool IsPath() => isPath;

}

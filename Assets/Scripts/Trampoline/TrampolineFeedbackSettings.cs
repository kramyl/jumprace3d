using UnityEngine;

[CreateAssetMenu(fileName = "TrampolineFeedback_Settings", menuName = "ScriptableObjects/Trampoline/Feedback", order = 1)]
public class TrampolineFeedbackSettings : ScriptableObject
{
    [Header("Settings")]
    public string feedbackPerfect = "Perfect!";
    public Color colorPerfect = Color.yellow;

    [Space]
    public string feedbackGreat = "Great!";
    public Color colorGreat = Color.blue;

    [Space]
    public string feedbackGood = "Good!";
    public Color colorGood = Color.green;

    [Space]
    public string feedbackLongJump = "Long Jump!";
    public Color colorLongJump = Color.magenta;
}

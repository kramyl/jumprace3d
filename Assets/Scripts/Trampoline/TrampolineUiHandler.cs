using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TrampolineUiHandler: MonoBehaviour
{
    [SerializeField, Header("References")]
    private TextMeshProUGUI textNumber;

    public void SetTextNumber(int number) => textNumber.text = number.ToString();
}

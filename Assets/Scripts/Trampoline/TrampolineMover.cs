using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineMover : MonoBehaviour
{
    [SerializeField]
    private float speed = 2f;
    [SerializeField]
    private float distance = 2f;

    private Vector3 targetPos;
    private Vector3 originPos;
    private float dir = 1;

    private Rigidbody rigid;

    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
        originPos = transform.position;
        targetPos = originPos + (transform.right * distance);
    }

    private void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, targetPos) >= 0.1f)
        {
            rigid.velocity = transform.right * dir * speed;
        }
        else
        {
            if (targetPos == originPos + (transform.right * -distance))
            {
                targetPos = originPos + (transform.right * distance);
                dir = 1;
            }
            else
            {
                targetPos = originPos + (transform.right * -distance);
                dir = -1;
            }
            
        }
       
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ContestantPositionUiHandler : MonoBehaviour
{
    [SerializeField, Header("Settings")]
    private Color colorAi = Color.white;
    [SerializeField]
    private Color colorPlayer = Color.white;

    [SerializeField, Header("Ui References")]
    private Image imgBg;
    [SerializeField]
    private TextMeshProUGUI textName;
    [SerializeField]
    private TextMeshProUGUI textNumber;

    public void Set(string name, int pos, bool isPlayer)
    {
        textName.text = name;
        textNumber.text = pos.ToString();
        imgBg.color = isPlayer ? colorPlayer : colorAi;
    }
}

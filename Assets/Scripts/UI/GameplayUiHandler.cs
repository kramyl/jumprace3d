using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayUiHandler : MonoBehaviour
{
    #region Singleton
    public static GameplayUiHandler Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

    }

    private void OnDestroy()
    {
        Instance = null;
    }
    #endregion

    public delegate void OnClickedStart();
    public event OnClickedStart onClickedStart;

    public delegate void OnClickedTapToContinue();
    public event OnClickedTapToContinue onClickedTapToContinue;

    [SerializeField, Header("UiReferences")]
    private Button buttonStart;
    [SerializeField]
    private Button buttonTapToContinue;
    [SerializeField, Space]
    private TextMeshProUGUI textCurrentLevel;
    [SerializeField]
    private TextMeshProUGUI textNextLevel;
    [SerializeField]
    private TextMeshProUGUI textFeedback;
    [SerializeField]
    private TextMeshProUGUI textRecapTitle;
    [SerializeField]
    private TextMeshProUGUI[] textRecapPlacements;
    [SerializeField, Space]
    private Image progressBarLevel;

    [SerializeField, Space]
    private RectTransform panelLevelCompleted;
    [SerializeField]
    private RectTransform panelFeedback;

    [SerializeField, Space]
    private ContestantPositionUiHandler[] contestantPositionUiHandlers;

    private const string recapTitleWin = "Level Completed";
    private const string recapTitleLose = "Level Failed";

    private void Start()
    {
        buttonStart.gameObject.SetActive(true);
        panelLevelCompleted.gameObject.SetActive(false);
        panelFeedback.gameObject.SetActive(false);
        buttonStart.onClick.AddListener(OnClickedButtonStart);
        buttonTapToContinue.onClick.AddListener(OnClickedButtonTapToContinue);
        SetProgressbarLevel(0);
        EnablePositions(false);
    }

    private void OnClickedButtonStart()
    {
        onClickedStart?.Invoke();
        EnablePositions();
        buttonStart.gameObject.SetActive(false);
    }

    private void OnClickedButtonTapToContinue()
    {
        onClickedTapToContinue?.Invoke();
    }

    private void EnablePositions(bool isEnabled = true)
    {
        foreach (var contestantPositionUiHandler in contestantPositionUiHandlers)
        {
            contestantPositionUiHandler.gameObject.SetActive(isEnabled);
        }
    }

    public void SetTextLevel(int currentLevel)
    {
        textCurrentLevel.text = currentLevel.ToString();
        textNextLevel.text = currentLevel++.ToString();
    }

    public void SetTextFeedback(string feedback, Color color)
    {
        panelFeedback.gameObject.SetActive(false);
        panelFeedback.gameObject.SetActive(true);
        textFeedback.text = feedback;
        textFeedback.color = color;
    }

    public void SetProgressbarLevel(float value) => progressBarLevel.fillAmount = value;

    public void ShowRecap(bool isLevelCompleted, string[] names)
    {
        buttonTapToContinue.gameObject.SetActive(true);
        textRecapTitle.text = isLevelCompleted ? recapTitleWin : recapTitleLose;
        for (int i = 0; i < textRecapPlacements.Length; i++)
        {
            textRecapPlacements[i].text = names[i];
        }
    }

    public void SetContestantPositions(string name, int pos, bool isPlayer)
    {

        var newIndex = pos;
        if (newIndex >= contestantPositionUiHandlers.Length)
            newIndex = contestantPositionUiHandlers.Length - 1;

        contestantPositionUiHandlers[newIndex].Set(name, pos + 1, isPlayer);
    }

}

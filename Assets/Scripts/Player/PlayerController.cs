using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private ContestantMotor playerMotor;
#if UNITY_ANDROID
    private Vector2 startPos;
#endif

    void Start()
    {
        GameplayUiHandler.Instance.onClickedStart += OnClickedStart;
        playerMotor = GameHandler.Instance.GetPlayerMotor();
    }

    void Update()
    {
#if UNITY_EDITOR
        playerMotor.SetIsMoving(Input.GetAxis("Vertical") != 0);
        var inputDir = Input.GetAxis("Horizontal");
#elif UNITY_ANDROID
       playerMotor.SetIsMoving(Input.touchCount == 1);
        var inputDir = 0f;
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.deltaPosition;
                    break;
                case TouchPhase.Moved:
                    inputDir = -((startPos - touch.deltaPosition).normalized.x);
                    break;
                case TouchPhase.Stationary:
                case TouchPhase.Ended:
                    inputDir = 0;
                    break;
            }

        }
#endif
        playerMotor.SetTurnDir(inputDir);
    }

    private void OnClickedStart()
    {
        GameHandler.Instance.IsRaceStarted = true;
        playerMotor.EnablePhysics(true);
    }
}


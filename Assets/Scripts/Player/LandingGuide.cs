using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ContestantMotor))]
public class LandingGuide : MonoBehaviour
{
    [SerializeField, Header("Settings")]
    private LayerMask groundLayer;
    [SerializeField]
    private Color colorHasPlatform = Color.green;
    [SerializeField]
    private Color colorNoPlatform = Color.red;
    [SerializeField]
    private int segmentCount = 60;
    [SerializeField]
    private bool isSimple = true;

    [SerializeField, Header("References")]
    private Transform landingIndicator;
    [SerializeField]
    private Transform landingIndicatorLine;
    [SerializeField]
    private MeshRenderer meshLandingIndicatorLine;

    private const float segmentScale = 1f;
    private const float maxDistance = 500f;

    private Rigidbody rigid;
    private ContestantMotor contestantMotor;

    private void Start()
    {
        colorNoPlatform = CopyAlpha(colorNoPlatform, meshLandingIndicatorLine.material.color);
        colorHasPlatform = CopyAlpha(colorHasPlatform, meshLandingIndicatorLine.material.color);
        contestantMotor = GetComponent<ContestantMotor>();
        rigid = GetComponent<Rigidbody>();
        landingIndicator.parent = null;
    }

    private void FixedUpdate()
    {
        if (!contestantMotor.IsGrounded())
        {
            if (isSimple)
                CheckSimpleLandingPosition();
            else
                CheckLandingPosition();
        }
        else
            landingIndicator.gameObject.SetActive(false);
    }

    private void CheckSimpleLandingPosition()
    {
        landingIndicator.gameObject.SetActive(true);
        Vector3 origin = transform.position;

        float hitDistance = maxDistance;
        Vector3 hitPos = Vector3.down * maxDistance;
        if (Physics.Raycast(origin, Vector3.down, out RaycastHit hit, maxDistance, groundLayer))
        {
            hitDistance = hit.distance;
            hitPos = hit.point;
            Debug.DrawLine(origin, hit.point);
        }
        meshLandingIndicatorLine.material.color = hitDistance == maxDistance ? colorNoPlatform : colorHasPlatform;

        hitPos.y += 0.02f;
        landingIndicator.position = hitPos;

        Vector3 newScale = landingIndicatorLine.localScale;
        newScale.y = hitDistance;
        landingIndicatorLine.localScale = newScale + (Vector3.up * .15f); ;
        landingIndicatorLine.position = transform.position;
    }

    private void CheckLandingPosition()
    {
        Vector3[] segments = new Vector3[segmentCount];

        segments[0] = transform.position;
        var segVelocity = rigid.velocity + contestantMotor.GetLastJumpForce() * Time.fixedDeltaTime;

        for (int i = 1; i < segmentCount; i++)
        {
            float segTime = (segVelocity.sqrMagnitude != 0) ? segmentScale / segVelocity.magnitude : 0;

            segVelocity = segVelocity + Physics.gravity * segTime;
            RaycastHit hit;
            if (Physics.Raycast(segments[i - 1], segVelocity, out hit, segmentScale, groundLayer))
            {
                segments[i] = segments[i - 1] + segVelocity.normalized * hit.distance;
                segVelocity = segVelocity - Physics.gravity * (1 - hit.distance) / segVelocity.magnitude; ;
                var position = hit.point;
                position.y += 0.2f;
                landingIndicator.position = position;
                landingIndicator.gameObject.SetActive(true);
            }
            else
            {
                segments[i] = segments[i - 1] + segVelocity * segTime;
            }
        }
    }

    private Color CopyAlpha(Color to, Color from)
    {
        return new Color(to.r, to.g, to.b, from.a);
    }

}

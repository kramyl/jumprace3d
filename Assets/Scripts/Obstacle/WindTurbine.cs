using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class WindTurbine : MonoBehaviour
{
    [SerializeField, Header("Settings")]
    private float speed = 5f;
    [SerializeField]
    private bool counterClockWise = false;

    Rigidbody rigid;
    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        var dir = transform.forward * -1f;
        if (counterClockWise)
        {
            dir = transform.forward;
        }
        rigid.angularVelocity = dir * speed;
    }
}
